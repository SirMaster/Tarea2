package com.umg.tarea.clases;

import java.util.List;

/**
 * Created by Guicho on 5/07/2017.
 */
public class Seccion {
    private String nombre;

    public Seccion() {
    }

    public Seccion(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
