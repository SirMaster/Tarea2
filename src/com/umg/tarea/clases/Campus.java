package com.umg.tarea.clases;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guicho on 5/07/2017.
 */
public class Campus {
    private List<Actividad> listadoActividad;

    public Campus() {
    }

    public Campus(List<Actividad> listadoActividad) {
        this.listadoActividad = listadoActividad;
    }

    public List<Actividad> getListadoActividad() {
        return listadoActividad;
    }

    public void setListadoActividad(List<Actividad> listadoActividad) {
        this.listadoActividad = listadoActividad;
    }
}
