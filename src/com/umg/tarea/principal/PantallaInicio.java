package com.umg.tarea.principal;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by lalopez on 12/07/2017.
 */
public class PantallaInicio extends JDialog {
    private JPanel contentPane;
    private JPanel panel1;
    private JButton addActividades;
    private JButton addColaborador;
    private JButton listActividades;
    private JButton cerrarButton;
    private DefaultTableModel tableModel;

    public PantallaInicio(){
        setContentPane(panel1);
        setModal(true);
        tableModel = new DefaultTableModel();
        addActividades.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IngresoActividades pantalla2 = new IngresoActividades();
                pantalla2.pack();
                pantalla2.setVisible(true);
            }
        });

        addColaborador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IngresoColaborador pantalla3 = new IngresoColaborador();
                pantalla3.pack();
                pantalla3.setVisible(true);
            }
        });

        cerrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    public static void main(String[] args) {
        PantallaInicio pantalla1 = new PantallaInicio();
        pantalla1.pack();
        pantalla1.setVisible(true);
        System.exit(0);
    }
}
