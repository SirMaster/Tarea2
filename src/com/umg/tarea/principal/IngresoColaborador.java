package com.umg.tarea.principal;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Guicho on 12/07/2017.
 */
public class IngresoColaborador extends JDialog {
    private JTextField nombre;
    private JTextField edad;
    private JTextField puesto;
    private JButton aceptarButton;
    private JButton cancelarButton;
    private JPanel panel3;
    private JTable table1;
    private JButton borrarButton;
    private JButton salirButton;
    private DefaultTableModel model;

    public IngresoColaborador() {
        setContentPane(panel3);
        setModal(true);

        model = new DefaultTableModel();
        model.addColumn("Nombre");
        model.addColumn("Edad");
        model.addColumn("Puesto");
        table1.setModel(model);

        cancelarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nombre.setText("");
                edad.setText("");
                puesto.setText("");
            }
        });
        aceptarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nombreVar = nombre.getText();
                int edadVar = Integer.parseInt(edad.getText());
                String puestoVar = puesto.getText();


                nombre.setText("");
                edad.setText("");
                puesto.setText("");

                model.addRow(new Object[]{nombreVar,edadVar,puestoVar});
            }
        });
        salirButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        borrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeRow(table1.getSelectedRow());
            }
        });
    }
}
