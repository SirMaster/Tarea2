package com.umg.tarea.principal;

import com.umg.tarea.clases.Actividad;
import com.umg.tarea.clases.Campus;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ContainerAdapter;
import java.util.List;

/**
 * Created by lalopez on 12/07/2017.
 */
public class IngresoActividades extends JDialog{
    private JPanel panel2;
    private JButton agregarButton;
    private JTable table1;
    private JButton cerrarButton;
    private JTextField actividad;
    private JButton borrarButton;
    private JButton aceptarButton;
    private JTable table2;
    private DefaultTableModel model, model2;

    Campus campus = new Campus();

    public IngresoActividades() {
        setContentPane(panel2);
        setModal(true);

        model = new DefaultTableModel();
        model.addColumn("Dentro del Campus");
        table1.setModel(model);

        model2 = new DefaultTableModel();
        model2.addColumn("Fuera del Campus");
        table2.setModel(model2);


        cerrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        agregarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String actividadVar = actividad.getText();

                actividad.setText("");

                    if (JOptionPane.showConfirmDialog(getParent(), "¿Es dentro del Campus?", "Aviso", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                        model.addRow(new Object[]{actividadVar});
                        Actividad actividad1 = new Actividad(actividad.getText());
                    }
                    else {
                        model2.addRow(new  Object[]{actividadVar});
                    }
                }
        });
        borrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeRow(table1.getSelectedRow());
                model2.removeRow(table2.getSelectedRow());
            }
        });
    }
}
